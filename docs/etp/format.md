---
title: 数据包格式
order: 200
---

## 数据包格式

| 字段     | 长度 |
| -------- | ---- |
| 版本号   | 1    |
| 控制字段 | 1    |
| 重传控制 | 1    |
| 加密方式 | 1    |
| 序列号   | 2    |
| 通信端口 | 2    |
| 消息体   | n    |

## 字段说明

#### 版本号

高 4 位为大版本号，接口不兼容。

低 4 位为小版本号，向下兼容。

#### 控制字段

| 位        | 说明                         | 备注                            |
| --------- | ---------------------------- | ------------------------------- |
| bit7-bit2 | 预留                         |                                 |
| bit1      | 目标设备是否需要回复响应数据 | 如果用到了丢包重传,该位必须置 1 |
| bit0      | 本数据是不是响应数据         |                                 |

#### 重传控制

- 高 4 位：重传次数
- 低 4 位：重传间隔(秒)

#### 加密方式

| 位        | 说明         | 备注 |
| --------- | ------------ | ---- |
| bit7-bit4 | 加密方式     |      |
| bit3      | 目标操作密钥 |      |
| bit2      | 目标设备密钥 |      |
| bit1      | 本机操作密钥 |      |
| bit0      | 本机设备密钥 |      |

#### 序列号

发送消息时的唯一序列号，每次发送消息后加 1。

应答消息时，序列号与被应答的消息的序列号相同。

#### 通信端口

- 0x8000 到 0xFFFF 为预留端口。
- 0x7000 到 0x7FFF 为公共端口，已定义好功能用法，所有设备必须遵守。
- 0x0000 到 0x6FFF 为私有端口，用户可以自定义功能。建议每个端口只负责一个功能。

#### 消息体

通信指令对应的参数
