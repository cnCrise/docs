---
nav:
  title: 统一网络消息协议
  order: 1000
title: 统一网络消息协议
order: 100
---

# 统一网络消息协议

unmp(Unified network message protocol),统一网络消息协议。  
通过 unmp-link(数据链路层)可以关联现有的通信协议(如 TCP、UDP、HTTP、MQTT、WebSocket、USART 等)。  
通过 unmp-net(网路层)可以根据设备 ID 建立起路由表，实现消息的中继转发。  
通过 unmp-protocol(传输层)可以自定义传输协议实现重传查重、分段重组等功能。  
通过 unmp-cloud(应用层)可以接入物联网云平台，享受云带来的便捷。

## 网络架构

![unmp网络架构](/assets/img/iotArchitecture.png)
