---
hero:
  title: 文档中心
  desc: 本网站记录了unmp及其子协议的开发者文档。
  actions:
    - text: 快速上手
      link: /unmp
features:
  - icon: https://gw.alipayobjects.com/zos/bmw-prod/881dc458-f20b-407b-947a-95104b5ec82b/k79dm8ih_w144_h144.png
    title: 开放共享
    desc: 不遗余力打造一个开放共享的互联网生态
  - icon: https://gw.alipayobjects.com/zos/bmw-prod/d60657df-0822-4631-9d7c-e7a869c2f21c/k79dmz3q_w126_h126.png
    title: 迅速开发
    desc: 站在巨人的肩膀上，迅速完成开发升级
footer: MIT Licensed | Copyright © 2018-present<br />Powered by [dumi](https://d.umijs.org)
---

### 概述

本文档介绍了unmp(统一网络消息协议)的使用方法。

注意： 此文章的代码，仅为用于示例调用和参数设置的代码片段，很可能有参数被引用，却未曾声明等情况，请开发者不必过于考究其中的细节。

### 联系我们

