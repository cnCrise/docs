---
title: C 语言
order: 920
---

## C 语言 SDK

### 下载

- 方式 1：使用 git 子模块(推荐)

```bash
git submodule add -b master https://gitee.com/rise0chen/riselib.git
git submodule add -b master https://gitee.com/cnCrise/unmp-net.git
git submodule add -b master https://gitee.com/cnCrise/unmp-protocol-etp.git
git submodule add -b master https://gitee.com/cnCrise/unmp-cloud.git
```

- 方式 2：直接下载
  [下载地址](/assets/attach/unmp/c.zip)

### 使用

#### 根据软硬件完成数据链路层代码

可参照[Linux UDP 链路层代码](https://gitee.com/unmp/example-linux/blob/master/lib/unmp-link/unmp_link_udp.c)  
需要自行完成以下函数

```c
#include "./unmp_link_udp.h"
static void unmp_link_udp_start(LinkServerStruct* self);
static void unmp_link_udp_send(LinkServerStruct* self, uint8_t* data, uint16_t len, LinkCfgStruct* link);

void startUdp(uint8_t* ip, uint32_t port) {
  LinkServerStruct link_server;
  LinkCfgStruct link;

  link.type = UnmpLinkTypeUdp;
  link.address = ip;
  link.port = port;

  link_server.link = &link;
  link_server.send = unmp_link_udp_send;
  link_server.recv = unmp_recv;
  link_server.fd = NULL;

  unmp_addServer(linkType_udp, &link_server);
  unmp_link_udp_start(unmp_getServer(linkType_udp));
}

void unmp_link_udp_start(LinkServerStruct* self) {
  LinkCfgStruct* link = self->link;

  //1、根据{link->port}、{link->address}初始化udp socket。
  //2、把udp socket保存到self->fd中。
  //3、启动udp socket接受数据相关的函数，如线程、中断、实时任务等。
}

void unmp_link_udp_send(LinkServerStruct* self, uint8_t* data, uint16_t dataLen, LinkCfgStruct* dstLink) {
  //通过self->fd，即udp socket，发送data到{dstLink->address}、{dstLink->port}
}

void* thread_recvData(void* x) {
  LinkServerStruct* self = (LinkServerStruct*)x;
  uint8_t recvBuf[256];
  uint16_t recvNum;

  //通过self->fd，即udp socket，获取数据

  //获取数据来源的ip地址与端口，并保存到LinkCfgStruct
  LinkCfgStruct srcLink;
  srcLink.type = linkType_udp;
  srcLink.address = (uint8_t*)&clientCfg.sin_addr.s_addr;
  srcLink.port = ntohs(clientCfg.sin_port);

  //调用self->recv，即unmp_recv
  self->recv(recvBuf, recvNum, &srcLink);
};
```

#### 初始化

1. `mempool_init(0x2000);`，初始化内存池，分配 8k 堆区内存。
1. `unmp_init(0);`，初始化 unmp 协议。
1. `unmp_setId(config.myId);`，设置设备 ID。如果未从云平台获取设备 ID，需先设置最高位为 1 的 8 字节 ID。
1. `startUdp(myIP, 0);`，启动本地 udp socket。（根据需求，设置数据链路层）
1. `unmp_connectCloud();`，连接域名"0.unmp.crise.cn"端口 16464 的公共 udp 服务器，即物联网云平台。（需根据不同设备自主实现本功能。如需使用私有化服务器，亦可直接连接私有化服务器）
1. `unmp_cloud_init(config.myId, config.myKey);`，初始化云平台服务。
1. `unmp_cloud_on(CloudEvent_error, (void*)onError);`，监听错误码。

#### 获取设备 ID

1. `unmp_cloud_getId(config.myTypeId, config.myTypeKey);`，使用设备类型 Id 与 Key 获取设备 ID 与 Key。
1. `unmp_cloud_on(cloudEvent_getId, (void*)onGetId);`，使用监听器保存返回的设备 ID 与 Key。

#### 登录

1. `unmp_cloud_login();`，登录物联网云平台。
1. `unmp_cloud_on(cloudEvent_login, (void*)onLogin);`，监听登录状态。
1. 登录成功后，建议:
   - 上传实际值变量`unmp_cloud_setActualVar`
   - 订阅变量变更通知`unmp_cloud_subTargetAllVar`
   - 开启 90s 定时器用于发送心跳包`unmp_cloud_heart();`

#### 进入绑定模式

1. `unmp_cloud_enterBinding(code);`，进入绑定模式。
1. `unmp_cloud_on(cloudEvent_enterBinding, (void*)onEnterBinding);`，监听返回的 4 字节绑定码。

#### 绑定设备

1. `unmp_cloud_bind(config.myTypeId, code);`，绑定指定设备类型 ID。
1. `unmp_cloud_on(cloudEvent_beBound, (void*)onBeBound);`，监听返回的设备 ID 与 Key。

### 参考示例

[git](https://gitee.com/cnCrise/unmp-example-linux.git)
