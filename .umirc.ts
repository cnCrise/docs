import { defineConfig } from 'dumi';

export default defineConfig({
  title: '文档中心',
  mode: 'site',
  hash: true,
  dynamicImport: {},
  history: { type: 'browser', options: { forceRefresh: false } },
  // more config: https://d.umijs.org/config
});
